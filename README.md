# How to Run

1. Install wrangler, using "yarn global add wrangler"
2. Run it locally using "wrangler dev"
3. Deploy it using "wrangler deploy"