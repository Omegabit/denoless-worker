import { Context } from "hono";
import { readAccessCookie } from "../../kiltapi/cookies";
import { Bindings } from "../../worker";

export async function checkCredential(context: Context<{ Bindings: Bindings }>): Promise<Response> {
  return await readAccessCookie(context, context.env.JWT_SIGNER_SECRET)
    .then((plainUserInfo) => {
      return context.text(plainUserInfo);
    })
    .catch((_e) => {
      return context.body(null, 204);
    });
}
