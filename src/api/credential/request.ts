import { Context } from "hono";
import { readSessionCookie } from "../../kiltapi/cookies";
import { Did, DidResourceUri, DidUri, EncryptCallback, EncryptResponseData, IEncryptedMessage, IMessage, IRequestCredential, IRequestCredentialContent, KiltEncryptionKeypair, Message } from "@kiltprotocol/sdk-js";
import { SessionValues, cookieOptions } from "../../kiltapi/types/api.types";
import { generateKeyPairs } from "../../kiltapi/keys";
import { setCookie } from "hono/cookie";
import { createJWT } from "../../kiltapi/handleJwt";
import { JWTPayload } from "jose";
import { Bindings } from "../../worker";

export async function requestCredential(context: Context<{ Bindings: Bindings }>): Promise<Response> {
  try {
    // read cookie from browser
    const sessionValues = await readSessionCookie(context, context.env.JWT_SIGNER_SECRET);

    if (!sessionValues.extension) {
      throw new Error(
        "Extension Session Values not found. Try restarting and verifying the server-extension-session.",
      );
    }

    // We need the encryptionKeyUri from the Extension
    const { did: claimerSessionDidUri } = Did.parse(
      sessionValues.extension.encryptionKeyUri,
    );

    // It is encouraged that you customize your challenge creation
    const challenge = crypto.randomUUID();

    const { attestation } = await context.req.json();

    const message = requestWrapper(
      attestation,
      challenge,
      claimerSessionDidUri,
      context.env.DAPP_DID_URI
    );

    await saveChallengeOnCookie(challenge, context);

    const encryptedMessage = await encryptMessage(message, sessionValues, context.env.DAPP_DID_MNEMONIC);

    return context.json(encryptedMessage);
  } catch (error: any) {
    console.error("Get Request Credential Error.", error);
    return context.json({ error: error.message }, { status: 500 });
  }
}

/** Turns the Credential Request into a Message.
 *  It also adds a challenge to the message for the claimer to signed.
 *  In this way, we make sure that the answer comes from who we asked.
 */
function requestWrapper(
  credentialRequest: Omit<IRequestCredentialContent, "challenge">,
  challenge: string,
  receiverDidUri: DidUri,
  DAPP_DID_URI: DidUri,
): IMessage {
  const messageBody: IRequestCredential = {
    content: { ...credentialRequest, challenge },
    type: "request-credential",
  };

  const message = Message.fromBody(messageBody, DAPP_DID_URI, receiverDidUri);
  return message;
}

/**
 * Protects from undesired readers.
 */
async function encryptMessage(
  message: IMessage,
  sessionObject: SessionValues,
  DAPP_DID_MNEMONIC: string,
): Promise<IEncryptedMessage> {
  const { keyAgreement: ourKeyAgreementKeyPair } = generateKeyPairs(
    DAPP_DID_MNEMONIC,
  );

  if (!sessionObject.extension) {
    throw new Error(
      "Receivers Encryption Key needed in order to encrypt a message",
    );
  }

  const ourKeyIdentifier = sessionObject.server.dAppEncryptionKeyUri;
  const theirKeyIdentifier = sessionObject.extension.encryptionKeyUri;

  const cypheredMessage = await Message.encrypt(
    message,
    encryptionCallback({
      keyAgreement: ourKeyAgreementKeyPair,
      keyAgreementUri: ourKeyIdentifier,
    }),
    theirKeyIdentifier,
  );

  return cypheredMessage;
}

async function saveChallengeOnCookie(
  challengeOnRequest: string,
  context: Context<{ Bindings: Bindings }>,
) {
  // Create a Json-Web-Token:
  // set the expiration of JWT same as the Cookie
  // Encode the completeSessionValues into a JWT:
  const payload: JWTPayload = JSON.parse(
    JSON.stringify({ challengeOnRequest }),
  );

  // default to algorithm: 'HS256',
  const token = await createJWT(payload);

  setCookie(context, "credentialJWT", token, cookieOptions);
}
