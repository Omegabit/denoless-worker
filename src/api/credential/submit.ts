import { IEncryptedMessage, Message, Credential, Attestation } from "@kiltprotocol/sdk-js";
import { Context } from "hono";
import { setCookie } from "hono/cookie";
import { fetchCtype } from "../../kiltapi/cache";
import { decryptionCallback } from "../../kiltapi/callbacks";
import { readCredentialCookie } from "../../kiltapi/cookies";
import { spiritnetAttesters } from "../../kiltapi/credentialContent";
import { getApi } from "../../kiltapi/getApi";
import { createJWT } from "../../kiltapi/handleJwt";
import { generateKeyPairs } from "../../kiltapi/keys";
import { cookieOptions } from "../../kiltapi/types/api.types";
import { Bindings } from "../../worker";

export async function submitCrdential(context: Context<{ Bindings: Bindings }>): Promise<Response> {
  try {
    const encryptedMessage = (await context.req.json()) as IEncryptedMessage;
    const { keyAgreement } = generateKeyPairs(context.env.DAPP_DID_MNEMONIC);
    const decryptedMessage = await Message.decrypt(
      encryptedMessage,
      decryptionCallback(keyAgreement),
    );
    const api = await getApi();
    // Verifying this is a properly-formatted message
    Message.verify(decryptedMessage);

    if (decryptedMessage.body.type !== "submit-credential") {
      throw new Error(`Unexpected message type: ${decryptedMessage.body.type}`);
    }

    // TODO:  maybe allow for several credentials in the future
    const credential = decryptedMessage.body.content[0];

    // Know against to what structure you want to compare to:
    // TODO: Fetch the cType requested in the POST
    const requestedCTypeHash =
      spiritnetAttesters.naturalPerson.cTypes[0].cTypeHash ||
      spiritnetAttesters.legalEntity.cTypes[0].cTypeHash;
    const requestedCTypeDetailed = await fetchCtype(requestedCTypeHash);

    // The function Credential.verifyPresentation can check against a specific cType structure.
    // This cType needs to match the ICType-interface.
    // To fullfil this structure we need to remove the 'creator' and 'createdAt' properties from our fetched object.
    const { $id, $schema, title, properties, type } = requestedCTypeDetailed;
    const requestedCType = { $id, $schema, title, properties, type };

    const challengeOnRequest = await readCredentialCookie(
      context,
      context.env.JWT_SIGNER_SECRET,
    );
    await Credential.verifyPresentation(credential, {
      challenge: challengeOnRequest,
      ctype: requestedCType,
    }).catch((error) => {
      return context.json({ error: error.message }, { status: 500 });
    });

    const attestationChain = await api.query.attestation.attestations(
      credential.rootHash,
    );

    const attestation = Attestation.fromChain(
      // @ts-ignore: The api.query returns a generic "Codec" type, but the SCALE decoded value is actually correct.
      attestationChain,
      credential.rootHash,
    );

    if (attestation.revoked) {
      throw new Error("Credential has been revoked and hence it's not valid.");
    }

    const ourTrustedAttesters = [
      ...spiritnetAttesters.naturalPerson.cTypes.flatMap(cType => cType.trustedAttesters),
      ...spiritnetAttesters.legalEntity.cTypes.flatMap(cType => cType.trustedAttesters)
    ];
    const attesterOfTheirCredential = attestation.owner;

    // If you don't include a list of trusted attester on the credential-request, this check would be skipped
    if (ourTrustedAttesters) {
      if (!ourTrustedAttesters.includes(attesterOfTheirCredential)) {
        console.error(
          `The Credential was not issued by any of the trusted Attesters that the dApp relies on. \n List of trusted attesters: ${ourTrustedAttesters}`,
        );
        return Promise.reject(
          `The Credential was not issued by any of the trusted Attesters that the dApp relies on. \n List of trusted attesters: ${ourTrustedAttesters}`,
        );
      }
    }

    // Send a little something to the frontend, so that the user interface can display who logged in.
    const plainUserInfo = credential.claim.contents
    const accessToken = await createJWT(plainUserInfo);

    setCookie(context, "accessJWT", accessToken, cookieOptions);
    return context.json(plainUserInfo);
  } catch (error: any) {
    console.error("Post Submit Credential Error.", error);
    return context.json({ error: error.message }, { status: 500 });
  }
}
