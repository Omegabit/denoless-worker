import { DidDocument, DidResourceUri } from "@kiltprotocol/sdk-js";
import { Context } from "hono";
import { setCookie } from "hono/cookie";
import { createJWT } from "../../kiltapi/handleJwt";
import { SessionValues, cookieOptions } from "../../kiltapi/types/api.types";
import { Bindings } from "../../worker";

export function generateSessionValues(didDocument: DidDocument, DAPP_NAME: string): SessionValues {
  // Build the EncryptionKeyUri so that the client can encrypt messages for us:
  const dAppEncryptionKeyUri = `${didDocument.uri}${didDocument.keyAgreement?.[0].id
    }` as DidResourceUri;

  if (didDocument.keyAgreement === undefined) {
    throw new Error("This DID has no Key Agreement. Cannot encrypt like this.");
  }

  // Generate a challenge to ensure all messages we receive are fresh.
  // A UUID is a universally unique identifier, a 128-bit label.
  const challenge = crypto.randomUUID();

  const sessionValues: SessionValues = {
    server: {
      dAppName: DAPP_NAME,
      dAppEncryptionKeyUri: dAppEncryptionKeyUri,
      challenge: challenge,
    },
  };
  return sessionValues;
}

/**
 * Saving the session values as a JSON-Web-Token on a Cookie of the browser
 */
export async function startSession(context: Context<{ Bindings: Bindings }>): Promise<Response> {
  // we use the DID-Document from the dApp fetched on server-start to generate our Session Values:
  const kv = await Deno.openKv();
  const { value: didDocument } = await kv.get<DidDocument>([DAPP_DID_URI]);
  if (didDocument === null) {
    // TODO: Probably it's better to fetch the DID-Document from the blockchain
    throw new Error("dappDidDocument not found in KV");
  }
  const sessionValues = generateSessionValues(didDocument, DAPP_NAME);
  const token = await createJWT(sessionValues);

  // Set a Cookie in the header including the JWT and our options:
  // Using 'cookie-parser' dependency:
  setCookie(context, "sessionJWT", token, cookieOptions);

  // send the Payload as plain text on the response, this facilitates the start of the extension session.
  return context.json(sessionValues);
}
