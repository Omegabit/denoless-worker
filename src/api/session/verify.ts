import { Context } from "hono";
import { readSessionCookie } from "../../kiltapi/cookies";
import { Did, Utils } from "@kiltprotocol/sdk-js";
import { setCookie } from "hono/cookie";
import { extractEncryptionKeyUri } from "../../kiltapi/extractEncryptionKeyUri";
import { getApi } from "../../kiltapi/getApi";
import { createJWT } from "../../kiltapi/handleJwt";
import { generateKeyPairs } from "../../kiltapi/keys";
import { SessionValues, cookieOptions } from "../../kiltapi/types/api.types";
import { Bindings } from "../../worker";

export async function verifySession(context: Context<{ Bindings: Bindings }>): Promise<Response> {
  // read cookie from browser
  const { server } = await readSessionCookie(context, context.env.JWT_SIGNER_SECRET);

  // Important/Real Verification:
  const { extensionSession } = await context.req.json();
  if (!extensionSession) {
    context.status(401);
    return context.text(
      "Session verification failed. No extension session values found.",
    );
  }
  const { encryptedChallenge, nonce } = extensionSession!;
  // This variable has different name depending on the session version that the extension uses
  const extensionEncryptionKeyUri = extractEncryptionKeyUri(extensionSession);

  await getApi();

  const encryptionKey = await Did.resolveKey(extensionEncryptionKeyUri);
  if (!encryptionKey) {
    throw new Error("an encryption key is required");
  }

  const { keyAgreement } = generateKeyPairs(context.env.DAPP_DID_MNEMONIC);

  const decryptedBytes = Utils.Crypto.decryptAsymmetric(
    { box: encryptedChallenge, nonce },
    // resolved from extension-URI
    encryptionKey.publicKey,
    // derived from your seed phrase:
    keyAgreement.secretKey,
  );
  // If it fails to decrypt, throw.
  if (!decryptedBytes) {
    /// COMMENT: Should we instead return a Premise.reject?
    throw new Error(
      "Could not decode/decrypt the challenge from the extension",
    );
  }

  const decryptedChallenge = new TextDecoder().decode(decryptedBytes);
  const originalChallenge = server.challenge;
  // Compare the decrypted challenge to the challenge you stored earlier.
  if (decryptedChallenge !== originalChallenge) {
    context.status(401);
    return context.text(
      "Session verification failed. The challenges don't match.",
    );
  }

  // update the cookie so that it also includes the extensionSession-Values
  const completeSessionValues: SessionValues = {
    server: {
      dAppName: server.dAppName,
      dAppEncryptionKeyUri: server.dAppEncryptionKeyUri,
      challenge: server.challenge,
    },
    extension: {
      encryptedChallenge: extensionSession.encryptedChallenge,
      encryptionKeyUri: extensionEncryptionKeyUri,
      nonce: extensionSession.nonce,
    },
  };

  // default to algorithm: 'HS256',
  const token = await createJWT(completeSessionValues);

  // Set a Cookie in the header including the JWT and our options:
  // Using 'cookie-parser' dependency:
  setCookie(context, "sessionJWT", token, cookieOptions);

  return context.text(
    "Session successfully verified. Extension and dApp understand each other. Server and Extension Session Values now on the Cookie.",
  );
}
