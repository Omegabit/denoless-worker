import {
  CType,
  CTypeHash,
  DidDocument,
  ICType,
} from "@kiltprotocol/sdk-js";
import { fetchDidDocument } from "./fetchDidDocument";
import { corroborateMyIdentity, validateOurKeys } from "./keys";

export const initkv = async (): Promise<void> => {
  const kv = await Deno.openKv();
  const res = await kv.get<DidDocument>([DAPP_DID_URI]);
  if (res.value === null) {
    const didDocument = await fetchDidDocument();
    await kv.set([DAPP_DID_URI], didDocument);
    validateOurKeys(didDocument);
  } else {
    validateOurKeys(res.value);
  }
  await corroborateMyIdentity(DAPP_DID_URI);
};

export const fetchCtype = async (
  requestedCTypeHash: CTypeHash,
): Promise<CType.ICTypeDetails> => {
  const kv = await Deno.openKv();
  const cTypeId: ICType["$id"] = `kilt:ctype:${requestedCTypeHash}`;
  const res = await kv.get<JSON>([cTypeId]);
  if (res.value === null) {
    const requestedCTypeDetailed = await CType.fetchFromChain(cTypeId).then(
      (cType) => JSON.stringify(cType),
    );
    await kv.set([cTypeId], requestedCTypeDetailed);
    return JSON.parse(requestedCTypeDetailed);
  } else {
    return res.value as unknown as CType.ICTypeDetails;
  }
};
