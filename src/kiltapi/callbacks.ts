import {
  DecryptCallback,
  DecryptResponseData,
  DidResourceUri,
  EncryptCallback,
  EncryptResponseData,
  KiltEncryptionKeypair,
} from "@kiltprotocol/types";
import { randomAsU8a } from "@polkadot/util-crypto";
import nacl from "tweetnacl";

type Sealed = {
  nonce: Uint8Array;
  sealed: Uint8Array;
};

function naclSeal(
  message: Uint8Array,
  senderBoxSecret: Uint8Array,
  receiverBoxPublic: Uint8Array,
  nonce: Uint8Array = randomAsU8a(24),
): Sealed {
  return {
    nonce,
    sealed: nacl.box(message, nonce, receiverBoxPublic, senderBoxSecret),
  };
}

function naclOpen(
  sealed: Uint8Array,
  nonce: Uint8Array,
  senderBoxPublic: Uint8Array,
  receiverBoxSecret: Uint8Array,
): Uint8Array | null {
  return (
    nacl.box.open(sealed, nonce, senderBoxPublic, receiverBoxSecret) || null
  );
}

export function encryptionCallback({
  keyAgreement,
  keyAgreementUri,
}: {
  keyAgreement: KiltEncryptionKeypair;
  keyAgreementUri: DidResourceUri;
}): EncryptCallback {
  return async function encryptCallback({
    data,
    peerPublicKey,
  }): Promise<EncryptResponseData> {
    const { sealed, nonce } = naclSeal(
      data,
      keyAgreement.secretKey,
      peerPublicKey,
    );
    return {
      nonce,
      data: sealed,
      keyUri: keyAgreementUri,
    };
  };
}

export function decryptionCallback(
  keyAgreement: KiltEncryptionKeypair,
): DecryptCallback {
  return async function decryptCallback({
    data,
    nonce,
    peerPublicKey,
  }): Promise<DecryptResponseData> {
    const decrypted = naclOpen(
      data,
      nonce,
      peerPublicKey,
      keyAgreement.secretKey,
    );

    if (!decrypted) {
      throw new Error("Failed to decrypt with given key");
    }

    return {
      data: decrypted,
    };
  };
}
