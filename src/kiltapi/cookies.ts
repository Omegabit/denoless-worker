import { Context } from "hono";
import { getCookie } from "hono/cookie";
import { jwtVerify } from "jose";
import { SessionValues } from "./types/api.types";
import { Did } from "@kiltprotocol/sdk-js";

/**
 *  Mean to facilitate working with the JSON-Web-Token inside the Cookie.
 *  This function reads, decodes and verifies the 'credentialJWT' Cookie from the browser.
 *  Verification in this context means that it will check, that the JWT was signed with the dApp's secret pen (key).
 *
 *  Will throw an error if it fail one of it functions.
 *
 * @param request
 * @param response
 * @param secretKey The JWT secret signer (or pen).
 * @returns The decoded content of the Payload of the JWT. Here the challenge from the cType-Request.
 */
export async function readCredentialCookie(
  context: Context<{ Bindings: Bindings }>,
  secretKey: Uint8Array,
): Promise<string> {
  // read cookie from browser
  const credentialCookie = getCookie(context, "credentialJWT");
  if (!credentialCookie) {
    context.status(401);
    return Promise.reject(
      `Could not find Cookie with challenge of the Credential-Request (as JWT). Enable Cookies and try again.`,
    );
  }

  try {
    // will throw error if verification fails
    const { payload } = await jwtVerify(credentialCookie, secretKey);
    // now make sure that the payload is carrying our type of SessionValues
    const challengeOnRequest: string = payload["challengeOnRequest"] as string;
    if (!challengeOnRequest) {
      return Promise.reject(
        "Challenge sent with the Credential-Request could not be extracted from the Cookie.",
      );
    } else {
      return challengeOnRequest;
    }
  } catch (error) {
    return Promise.reject(`Could not verify JWT. ${error}`);
  }
}

/**
 *  Mean to facilitate working with the JSON-Web-Token inside the Cookie.
 *  This function reads, decodes and verifies the 'sessionJWT' Cookie from the browser.
 *  Verification in this context means that it will check, that the JWT was signed with the dApp's secret pen (key).
 *
 *  Will throw an error if it fail one of it functions.
 *
 * @param request
 * @param response
 * @param secretKey The JWT secret signer (or pen).
 * @returns The decoded content of the Payload of the JWT. Here the session values.
 */
export async function readSessionCookie(
  context: Context<{ Bindings: Bindings }>,
  secretKey: Uint8Array,
): Promise<SessionValues> {
  // read cookie from browser
  const sessionCookie = getCookie(context, "sessionJWT");
  if (!sessionCookie) {
    return Promise.reject("No session cookie found.");
  }

  // decode the JWT and verify if it was signed with our SecretKey

  try {
    // will throw error if verification fails
    const { payload } = await jwtVerify(sessionCookie, secretKey);
    // now make sure that the payload is carrying our type of SessionValues
    // extract the session.server and session.extension Objects from payload
    const { server, extension } = payload as SessionValues;

    if (!server) {
      throw new Error(
        "Server Session Values could not be extracted from the Cookie.",
      );
    }

    checkSessionValuesTypes(server, extension);

    // after our check, we can cast the Object as SessionValues with a clean conscience
    const sessionObject = { server, extension };
    return sessionObject;
  } catch (error) {
    throw new Error(`Could not verify JWT. ${error}`);
  }
}

/**
 *  Mean to facilitate working with the JSON-Web-Token inside the Cookie.
 *  This function reads, decodes and verifies the 'accessJWT' Cookie from the browser.
 *  Verification in this context means that it will check, that the JWT was signed with the dApp's secret key (more like a secret pen)..
 *
 *  Will throw an error if it fail one of it functions.
 *
 * @param request
 * @param response
 * @param secretKey The JWT secret signer (or pen).
 * @returns The decoded content of the Payload of the JWT. Here the user's authentication token.
 */
export async function readAccessCookie(
  context: Context<{ Bindings: Bindings }>,
  secretKey: Uint8Array,
): Promise<string> {
  // read cookie from browser
  const accessCookie = getCookie(context, "accessJWT");
  if (!accessCookie) {
    return Promise.reject("No access cookie found.");
  }

  try {
    // will throw error if verification fails
    const { payload } = await jwtVerify(accessCookie, secretKey);
    const { sub } = payload;
    if (!sub) {
      return Promise.reject("No authentication Token found.");
    }
    return sub;
  } catch (error) {
    return Promise.reject(`Could not verify JWT. ${error}`);
  }
}

/**
 * Checks that types fits the SessionValues Interface
 *
 * Will throw if any property does not matches expectation.
 * @param server -- session.server object
 * @param extension -- session.extension object (optional)
 */
function checkSessionValuesTypes(
  server: Record<string, unknown>,
  extension?: Record<string, unknown>,
) {
  // Cheek the session.server:

  if (typeof server !== "object" || server === null) {
    throw new Error(
      "The server session values are not packed in an object as expected. ",
    );
  }

  areTheyStrings(server, ["dAppName", "challenge"]);

  if ("dAppEncryptionKeyUri" in server) {
    Did.validateUri(server.dAppEncryptionKeyUri, "ResourceUri");
  } else {
    throw new Error(
      "Property 'dAppEncryptionKeyUri' of session.server could not be found",
    );
  }

  // if the extension session values are not there yet, stop here.
  if (!extension) {
    return;
  }

  // Check the session.extension

  if (typeof extension !== "object" || extension === null) {
    throw new Error(
      "The extension session values are not packed in an object as expected. ",
    );
  }

  areTheyStrings(extension, ["encryptedChallenge", "nonce"]);

  if ("encryptionKeyUri" in extension) {
    Did.validateUri(extension.encryptionKeyUri, "ResourceUri");
  } else {
    throw new Error(
      "Property 'encryptionKeyUri' of session.extension could not be found",
    );
  }
}

/**
 * Generalizes the `for...in`-loops.
 *
 * @param subSession - either session.server or session.extension
 * @param keyNames - array of strings with the name of the corresponding properties.
 */
function areTheyStrings(
  subSession: Record<string, unknown>,
  keyNames: string[],
) {
  for (const property of keyNames) {
    if (!(property in subSession)) {
      throw new Error(
        `Property '${property}' of session.server object could not be found.`,
      );
    }
    if (!(typeof subSession[property] == "string")) {
      throw new Error(
        `Property '${property}' of session.server object should be of type 'string'.
           Instead it is of type: ${typeof subSession[property]}`,
      );
    }
  }
}
