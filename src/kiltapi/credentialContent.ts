import {
  DidUri,
  IRequestCredentialContent,
} from "@kiltprotocol/types";

// these are for peregrine
interface ISpiritnetAttesters {
  naturalPerson: IRequestCredentialContent;
  legalEntity: IRequestCredentialContent;
}

export const spiritnetAttesters: ISpiritnetAttesters = {
  naturalPerson: {
    cTypes: [
      {
        cTypeHash:
          // TODO: replace with the correct hash
          "0x866cdc4edd6f06a4e566b34940b2ece90440f58bcbca7b23d4aed3e2f43525b4",
        trustedAttesters: [
          // TODO: replace with the correct did
          "did:kilt:4rj9qdTm52aVyDPtD9Dwzi4aQC2T9FXrrfWez3bNnp4YpqHb" as DidUri,
        ],
        // find properties in libs/auth/src/credential/credentials.types.ts
        // TODO: replace with the correct properties
        // requiredProperties: ['email'],
      },
    ],
  },
  legalEntity: {
    cTypes: [
      {
        cTypeHash:
          // TODO: replace with the correct hash
          "0x1619e508121dc2bd8299d4da939b566748aeea4bbb9527510cac1a7e68ef3b4d",
        trustedAttesters: [
          // TODO: replace with the correct did
          "did:kilt:4rj9qdTm52aVyDPtD9Dwzi4aQC2T9FXrrfWez3bNnp4YpqHb" as DidUri,
        ],
        // find properties in libs/auth/src/credential/credentials.types.ts
        // TODO: replace with the correct properties
        // requiredProperties: ['email'],
      },
    ],
  },
};
