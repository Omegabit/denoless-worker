import {
    Did,
    DidDocument,
    disconnect,
  } from "@kiltprotocol/sdk-js";
  
  import { DAPP_DID_URI } from "./env";
  import { getApi } from "./getApi";
  
  /**
   * This function is to fetch the the DID-Document of your dApp from the Blockchain once.
   *
   * It is meant to be used before starting the server so that the document is available to use.
   *
   * @returns didDocument Kilt.DidDocument
   */
  export async function fetchDidDocument(): Promise<DidDocument> {
    // connects to the websocket of your, in '.env', specified blockchain
    await getApi();
  
    if (!DAPP_DID_URI) {
      throw new Error("enter your dApp's DID URI on the .env-file first");
    }
  
    // fetch the DID document from the blockchain
    const resolved = await Did.resolve(DAPP_DID_URI);
  
    // Assure this did has a document on chain
    if (resolved === null) {
      throw new Error("DID could not be resolved");
    }
    if (!resolved.document) {
      throw new Error(
        "No DID document could be fetched from your given dApps URI",
      );
    }
    const didDocument = resolved.document;
    // We require a key agreement key to receive encrypted messages
    if (!didDocument.keyAgreement || !didDocument.keyAgreement[0]) {
      throw new Error(
        "The DID of your dApp needs to have an Key Agreement to communicate. Info to get one: https://docs.kilt.io/docs/develop/sdk/cookbook/dids/full-did-update",
      );
    }
    if (!didDocument.authentication || !didDocument.authentication[0]) {
      throw new Error(
        "The DID of your dApp needs to have an authentication Key to sign stuff. Info to get one: https://docs.kilt.io/docs/develop/sdk/cookbook/dids/full-did-update",
      );
    }
  
    await disconnect();
  
    return didDocument;
  }
  