import { JWTPayload, SignJWT } from "jose";
import { cookieOptions } from "./types/api.types";
import { DAPP_DID_URI, JWT_SIGNER_SECRET } from "./env";

export async function createJWT(
  payload: JWTPayload,
  subject?: string,
): Promise<string> {
  let token = new SignJWT(payload) // details to  encode in the token
    .setProtectedHeader({ alg: "HS256" }) // algorithm
    .setIssuedAt()
    .setIssuer(DAPP_DID_URI) // issuer
    .setExpirationTime(`${cookieOptions.maxAge} seconds`); // set the expiration of JWT same as the Cookie

  if (subject) {
    token = token.setSubject(subject); // subject
  }

  return await token.sign(JWT_SIGNER_SECRET);
}
