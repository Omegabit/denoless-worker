import {
  DidDocument,
  DidKey,
  DidUri,
  KeyRelationship,
  KeyringPair,
  KiltEncryptionKeypair,
  KiltKeyringPair,
  Utils,
} from '@kiltprotocol/sdk-js';
import { u8aEq } from '@polkadot/util';
import {
  blake2AsU8a,
  keyExtractPath,
  keyFromPath,
  mnemonicToMiniSecret,
  sr25519PairFromSeed,
} from '@polkadot/util-crypto';
import { VerifiableDomainLinkagePresentation } from './types/kilt.types';
import { Context } from 'hono';

/**
 * This function checks that the public keys linked to our DID on chain match the ones we generate now.
 *
 * This would fail if the keys derivation path has changed.
 *
 * If this fails, it means you can not sign or encrypt anything that could be verified or decrypted by your counterpart.
 *
 * @param didDocument
 */
export function validateOurKeys(didDocument: DidDocument, DAPP_DID_MNEMONIC: string) {
  const localKeyPairs = generateKeyPairs(DAPP_DID_MNEMONIC);

  // A DID can have several keys of type 'keyAgreement', but only up to one key of each of the other types.
  // All DIDs need to have an 'authentication' key, so this first assertion should never fail
  const necessaryTypesOfKeys: KeyRelationship[] = [
    'authentication',
    'assertionMethod',
    'keyAgreement',
  ];

  for (const keyName of necessaryTypesOfKeys) {
    compareKey(didDocument[keyName]?.[0], localKeyPairs[keyName], keyName);
  }
  // don't throw if 'capabilityDelegation' key is missing because it is not really necessary for this project
  const trivialKey: KeyRelationship = 'capabilityDelegation';
  try {
    compareKey(
      didDocument[trivialKey]?.[0],
      localKeyPairs[trivialKey],
      trivialKey
    );
  } catch (warning) {
    console.warn(
      `Non-essential Key "${trivialKey}" not available. Reason: "${warning}" `
    );
  }
}

function compareKey(
  resolved: DidKey | undefined,
  derived: KeyringPair | KiltEncryptionKeypair,
  relationship: KeyRelationship
): void {
  if (!resolved) {
    throw new Error(`No "${relationship}" Key for your DID found on chain.`);
  }

  if (!u8aEq(derived.publicKey, resolved.publicKey)) {
    throw new Error(
      `Public "${relationship}" Key on chain does not match what we are generating.`
    );
  }
}

/**
 * Check if the **Well-Known DID Configuration** being displayed on the website uses the same DID URI as the one on the `.env`-file.
 *
 * If the DID URIs do not match, the extensions would not trust us. Your ID would be saying a different name as what you claim.
 *
 * @param DAPP_DID_URI
 */
export async function corroborateMyIdentity(dAppDidUri: DidUri, contect: Context) {
  contect.MY_KV_NAMESPACE.get("well-known-did-configuration.json");
  if (!fileContent) {
    throw new Error(
      'The well-known-did-configuration file found on your repository is empty.'
    );
  }
  const wellKnownDidConfig = JSON.parse(
    fileContent
  ) as VerifiableDomainLinkagePresentation;



  const didExists = wellKnownDidConfig.linked_dids.some(
    linked_did => linked_did.credentialSubject.id === dAppDidUri
  );

  if (!didExists) {
    throw new Error(`
        The 'Well-Known DID Configuration' that your dApp displays was issued with a different DID than the one that the server has at disposition.
        
        The DID as environment constant: ${dAppDidUri}`);
  }
}

function generateKeyAgreement(mnemonic: string) {
  const secretKeyPair = sr25519PairFromSeed(mnemonicToMiniSecret(mnemonic));
  const { path } = keyExtractPath('//did//keyAgreement//0');
  const { secretKey } = keyFromPath(secretKeyPair, path, 'sr25519');
  return Utils.Crypto.makeEncryptionKeypairFromSeed(blake2AsU8a(secretKey));
}

export function generateKeyPairs(mnemonic: string) {
  // Currently, the default the key type used by the Kilt-team is "sr25519". Better to use it for compatibility.
  const account = Utils.Crypto.makeKeypairFromSeed(
    mnemonicToMiniSecret(mnemonic),
    'sr25519'
  );

  // You can derive the keys however you want to and it will still work.
  // But if, for example, you try to load your seed phrase in a third party wallet, you will get a different set of keys, because the derivation is different.
  // For a start, it is better to use the same derivations as Sporran. So you can load your Accounts and DIDs there and check if everything worked fine.

  const authentication = account.derive('//did//0') as KiltKeyringPair;

  const assertionMethod = account.derive(
    '//did//assertion//0'
  ) as KiltKeyringPair;

  // the delegation Keys are not needed for this project.
  const capabilityDelegation = account.derive(
    '//did//delegation//0'
  ) as KiltKeyringPair;

  // The encryption keys, a.k.a. keyAgreement, are not natively supported by the Polkadot library.
  // So to derive this kinds of keys, we have to play a bit with lower-level details.
  // That's whats done in the extra function generateKeyAgreement()

  const keyAgreement = generateKeyAgreement(mnemonic);

  return {
    authentication: authentication,
    keyAgreement: keyAgreement,
    assertionMethod: assertionMethod,
    capabilityDelegation: capabilityDelegation,
  };
}
