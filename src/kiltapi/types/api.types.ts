import type { CookieOptions } from "hono/utils/cookie";
import { JWTPayload } from "jose";
import { DidResourceUri } from "@kiltprotocol/sdk-js";
/**
 * Define how the Session Values are packaged.
 *
 * At the start, we only have the values from the server.
 * After verification, we also save the values that the extension (wallet) send us.
 */
export interface SessionValues extends JWTPayload {
  server: {
    dAppName: string;
    dAppEncryptionKeyUri: DidResourceUri;
    challenge: string;
  };
  extension?: {
    encryptedChallenge: string;
    encryptionKeyUri: DidResourceUri;
    nonce: string;
  };
}

// Set Cookie Options: (list of ingredients)
export const cookieOptions: CookieOptions = {
  // Indicates the number of seconds until the Cookie expires.
  maxAge: 60 * 60 * 24,
  // only send over HTTPS
  secure: true,
  // TODO: Set to "Strict" to prevent cross-site request forgery attacks
  sameSite: "None",
  // restricts URL that can request the Cookie from the browser. '/' works for the entire domain.
  path: "/",
  // Forbids JavaScript from accessing the cookie
  httpOnly: true,
};
