export declare const DEFAULT_VERIFIABLECREDENTIAL_CONTEXT =
  "https://www.w3.org/2018/credentials/v1";
import { DidUri, Hash } from "@kiltprotocol/sdk-js";
import {
  SelfSignedProof,
  VerifiableCredential,
} from "@kiltprotocol/vc-export";

export interface CredentialSubject {
  id: DidUri;
  origin: string;
  rootHash: Hash;
}

const context = [
  DEFAULT_VERIFIABLECREDENTIAL_CONTEXT,
  "https://identity.foundation/.well-known/did-configuration/v1",
];

export interface DomainLinkageCredential extends
  Omit<
    VerifiableCredential,
    "@context" | "legitimationIds" | "credentialSubject" | "proof"
  > {
  "@context": typeof context;
  credentialSubject: CredentialSubject;
  proof: SelfSignedProof;
}

export interface VerifiableDomainLinkagePresentation {
  "@context": string;
  linked_dids: [DomainLinkageCredential];
}
