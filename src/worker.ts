export type Bindings  = {
	// Example binding to KV. Learn more at https://developers.cloudflare.com/workers/runtime-apis/kv/
	MY_KV_NAMESPACE: KVNamespace;
	JWT_SIGNER_SECRET: Uint8Array;
	DAPP_DID_URI: DidUri;
	WSS_ADDRESS: string;
	DAPP_NAME: string;
	DAPP_ACCOUNT_MNEMONIC: string;
	DAPP_DID_MNEMONIC: string;
}

import { Hono } from 'hono'
import { logger } from 'hono/logger'
import { cors } from 'hono/cors'
import { deleteCookie } from 'hono/cookie'
import { getApi } from './kiltapi/getApi';
import { cookieOptions } from './kiltapi/types/api.types';
import { DidUri } from '@kiltprotocol/sdk-js';
import { checkCredential, requestCredential, startSession, submitCrdential, verifySession } from './api';

const app = new Hono<{ Bindings: Bindings }>()
const api = new Hono<{ Bindings: Bindings }>()


const corsOptions = {
	origin: (origin: string) => {
		if (origin.includes('localhost')) {
			return origin;
		} else {
			// FIXME: We cannout use a Regex. We need to use a string.
			// return /\Ahttps?:\/\/([a-zA-Z\d-]+\.){0,}polimec\.org\/?\z/;
			return "https://credentials.polimec.org"
		}
	},
	methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
	maxAge: 86400,
	credentials: true,
};

app.get('/', (c) => {
	return c.text('Hello from /!');
});

api.get('/', (c) => {
	return c.text('Hello from /api!');
});

api.get('/session/start', async (c) => {
	await getApi();
	return await startSession(c);
});

api.post('/session/verify', async (c) => {
	await getApi();
	return await verifySession(c);
});

api.post('/credential/request', async (c) => {
	await getApi();
	return await requestCredential(c);
});

api.post('/credential/submit', async (c) => {
	await getApi();
	return await submitCrdential(c);
});

api.get('/credential/check', async (c) => {
	await getApi();
	return await checkCredential(c);
});

api.post('/logout', (c) => {
	deleteCookie(c, 'sessionJWT', cookieOptions);
	deleteCookie(c, 'credentialJWT', cookieOptions);
	deleteCookie(c, 'accessJWT', cookieOptions);
	return c.body(null, 200);
});

app.use('*', logger());
// TODO: Check if we the CORS settings are correct
app.use('*', cors(corsOptions));
app.route('/api', api);

export default app